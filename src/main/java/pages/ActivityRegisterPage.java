/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

/**
 *
 * @author accurate
 */
public class ActivityRegisterPage {

//    RemoteWebDriver driver;
    private final String pageUrl = "https://lab.accurate.com.br/accweb/ativ_inclusao.asp";
    private WebElement cboRecurso;
    private WebElement txtData;
    private WebElement cboCliContrProj;
    private Select cboCliContrProj_select;
    private WebElement cboEtapa;
    private Select cboEtapa_select;
    private WebElement cboFase;
    private Select cboFase_select;
    private WebElement txtAtividade;
    private WebElement txtHoraInicio;
    private WebElement txtQtHora;
    private WebElement btIncluir;

    public ActivityRegisterPage() {
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public WebElement getCboRecurso() {
        return cboRecurso;
    }

    public WebElement getTxtData() {
        return txtData;
    }

    public WebElement getCboCliContrProj() {
        return cboCliContrProj;
    }

    public Select getCboCliContrProj_select() {
        cboCliContrProj_select = new Select(cboCliContrProj);
        return cboCliContrProj_select;
    }

    public Select getCboEtapa_select() {
        cboEtapa_select = new Select(cboEtapa);
        return cboEtapa_select;
    }

    public WebElement getCboEtapa() {
        return cboEtapa;
    }

    public WebElement getCboFase() {
        return cboFase;
    }

    public Select getCboFase_select() {
        cboFase_select = new Select(cboFase);
        return cboFase_select;
    }

    public WebElement getTxtAtividade() {
        return txtAtividade;
    }

    public WebElement getTxtHoraInicio() {
        return txtHoraInicio;
    }

    public WebElement getTxtQtHora() {
        return txtQtHora;
    }

    public WebElement getBtIncluir() {
        return btIncluir;
    }
}
