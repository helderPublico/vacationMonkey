/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

import org.openqa.selenium.WebElement;

/**
 *
 * @author accurate
 */
public class LoginPage {

//    private static final String pageUrl = "https://lab.accurate.com.br/request/login.jsp";
//    private static final String pageUrl = "https://lab.accurate.com.br/request/login.jsp?os_destination=/loginAccProjetos.jsp&URL=%2Faccweb%2Fresumo%2Easp%3F&Msg=Voc%EA+n%E3o+efetuou+login+ou+sua+sess%E3o+expirou%2E%3Cbr%3E%C9+preciso+fazer+login+para+acesso+a+este+m%F3dulo&aplic=Acompanhamento+de+Projetos";
    private static final String pageUrl = "https://lab.accurate.com.br/accweb";

    private WebElement os_username;
    private WebElement os_password;
    private WebElement login;

    public LoginPage() {
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public WebElement getOs_username() {
        return os_username;
    }

    public WebElement getOs_password() {
        return os_password;
    }

    /**
     * botao login
     */
    public WebElement getLogin() {
        return login;
    }
}
