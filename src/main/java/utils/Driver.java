/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Singleton double checked lock.
 *
 * @author accurate
 *
 *
 */
public class Driver extends RemoteWebDriver {

    private volatile static RemoteWebDriver uniqueInstance;

    private Driver() {
    }

    public static RemoteWebDriver getInstance() {

        if (uniqueInstance == null) {

            //synchornized only first time
            synchronized (Driver.class) {

                if (uniqueInstance == null) {

                    FirefoxOptions ffOptions = new FirefoxOptions();
                    // comente essa linha para ver a janela do navegador
                    ffOptions.addArguments("--headless");

                    DesiredCapabilities desiredCap = DesiredCapabilities.firefox();
                    desiredCap.setBrowserName("firefox");
                    desiredCap.setPlatform(org.openqa.selenium.Platform.ANY);
                    desiredCap.setCapability(FirefoxOptions.FIREFOX_OPTIONS, ffOptions);

                    try {
                        uniqueInstance = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), desiredCap);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        System.out.println("\n\n------- remember to start selenium server. ------------");
                        e.printStackTrace();
                    }
                }

            }

        }

        return uniqueInstance;
    }
}
