package vacation;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import pages.LoginPage;
import pages.ActivityRegisterPage;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import utils.Driver;

/**
 *
 * @author accurate
 */
public class VacationMonkey {

    RemoteWebDriver driver;

    public VacationMonkey() {
        driver = Driver.getInstance();
    }

    public void doLogin(String user, String password) {

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);

        driver.get(loginPage.getPageUrl());
        loginPage.getOs_username().sendKeys(user);
        loginPage.getOs_password().sendKeys(password);
        loginPage.getLogin().click();
    }

    public void fillVacation(LocalDate startDate, LocalDate endDate) throws InterruptedException {
        ActivityRegisterPage activityRegisterPage = PageFactory.initElements(driver, ActivityRegisterPage.class);

        driver.get(activityRegisterPage.getPageUrl());

        while (startDate.isBefore(endDate) || startDate.equals(endDate)) {

            if (!startDate.getDayOfWeek().equals(DayOfWeek.SATURDAY) && !startDate.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {

                System.out.print(startDate + " " + startDate.getDayOfWeek() + "\n");

                activityRegisterPage.getTxtData().clear();
                activityRegisterPage.getTxtData().sendKeys(startDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                // 22	217	81 = accurate / -accFerAfast / AccGes
                activityRegisterPage.getCboCliContrProj_select().selectByValue("22	217	81");
                activityRegisterPage.getCboEtapa_select().selectByValue("FERIAS");
//                activityRegisterPage.getCboFase_select().selectByValue("20"); // 20 = comercial
                activityRegisterPage.getCboFase_select().selectByValue("3"); // 3 = ferias
                activityRegisterPage.getTxtAtividade().clear();
                activityRegisterPage.getTxtAtividade().sendKeys("Férias");
                activityRegisterPage.getTxtHoraInicio().clear();
                activityRegisterPage.getTxtHoraInicio().sendKeys("08:00");
                activityRegisterPage.getTxtQtHora().clear();
                activityRegisterPage.getTxtQtHora().sendKeys("8");
                activityRegisterPage.getBtIncluir().click();

            }

            startDate = startDate.plusDays(1);
        }

    }
}
