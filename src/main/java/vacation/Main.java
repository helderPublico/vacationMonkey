/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vacation;

import java.io.Console;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Scanner;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.openqa.selenium.remote.RemoteWebDriver;
import utils.Driver;

/**
 *
 * @author accurate
 */
public class Main {

    private static String user;
    private static char[] password;
    private static LocalDate startDate = null;
    private static LocalDate endDate = null;

    public static void main(String[] args) {

        try {
            fillVacation();
        } catch (Exception e) {
            e.printStackTrace();
        }

//        System.out.println(user);
//        System.out.println(String.valueOf(password));
//        System.out.println(startDate.toString());
//        System.out.println(endDate.toString());
//        RemoteWebDriver driver = Driver.getInstance();
    }

    private static void fillVacation() throws Exception {

        getParams();

        VacationMonkey vacationMonkey = new VacationMonkey();
        vacationMonkey.doLogin(user, String.valueOf(password));
        vacationMonkey.fillVacation(startDate, endDate);
    }

    private static void getParams() throws Exception {
        String startDateAux = null;
        String endDateAux = null;
        Scanner sc = new Scanner(System.in);
        Console console = System.console();

        if (console == null) {
            System.out.println("\n\nRun the jar on terminal to get a Console object.");
            throw new Exception("Console not found.");
        }

        System.out.print("Accweb user name: ");
        user = sc.nextLine();
//        user = "seuUsario";

        System.out.print("Accweb user password: ");
        password = System.console().readPassword();
//        char[] password2 = {'s', 'e', 'n', 'h', 'a', 'a', 'q'};
//        password = password2;

        System.out.print("Start date (YYYY-MM-DD): ");
        startDateAux = sc.nextLine();
//        startDateAux = "2018-03-16";

        System.out.print("End date (YYYY-MM-DD): ");
        endDateAux = sc.nextLine();
//        endDateAux = "2018-03-19";

        startDate = LocalDate.parse(startDateAux);
        endDate = LocalDate.parse(endDateAux);

    }
}
